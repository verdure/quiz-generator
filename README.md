# Quiz Generator

A program that generates a quiz of 10 questions from a file containing a selection of questions. The program checks the answers and gives the user a final score.