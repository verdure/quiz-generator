import random
import collections

# Open the file containing the selection of questions and split it
def get_questions(filename):
    with open(filename) as f:
        # Strip new line characters
        lines = (line.rstrip() for line in f)
        lines = list(line for line in lines if line)
        # Group the file's contents into blocks of six (since six lines comprise
        # a full question) and shuffle them
        questions = [lines[i:i + 6] for i in range(0, len(lines), 6)]
        random.shuffle(questions)
        print(questions)
        # Format questions into separate categories of questions, answers and 
        # correct answers
        question = collections.namedtuple('Question', 
                                            'question, answers, correct')
        formatted_questions = [question(*i[:1], i[1:5], *i[5:]) for i in questions]
        print(formatted_questions)
        # Group questions into groups of 10
        group_questions = [formatted_questions[i:i + 10] for i in range(0, len(formatted_questions), 10)]

# Generate the questions
get_questions("quiz.txt")
